﻿$(document).ready(function () {
    if ($("#receiverInfo").val() == "" || $("#comDetailList").val() == "")
    {
        $("#btnSave").attr("disabled", true);
    }
});

//新增出货单
function SaveShipInfo() {
    if ($("#SHIP_TIME").val() == "")
    {
        alert("出货日期不能为空!");
        $("#SHIP_TIME").focus();
        return;
    }
    var jsondata = GetData();
    if (confirm("确认保存吗?")) {

        $.post('/ShipAdd/SaveShip', { shipInfo: jsondata }, function (data) {
            alert(data);
            },
            "json"
            )
    }
}


//获取数据
function GetData() {
    
    var ids = getQueryString("IDS");

    var idsArray = new Array();
    idsArray = ids.split(",");
    for (var j = 0, max = idsArray.length; j < max; j++) {
        idsArray[j] = {
            "PRE_PRODUCE_ID": parseInt(idsArray[j])
        }
    }

    var AddData = "{";
    //出货单
    AddData += '"SHIP_TIME":"' + $("#SHIP_TIME").val() + '",';

    //备注
    AddData += '"REMARK":"' + $("#txtRemark").val() + '",';


    //预生产单ID
    AddData += '"detailList":' + JSON.stringify(idsArray) + '';

    AddData += "}";

    return AddData;
};

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}