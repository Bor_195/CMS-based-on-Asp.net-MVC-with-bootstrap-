﻿//获取数据
function GetData() {

    var id = getQueryString("ID");
    
    var ddl = document.getElementById("drpstaff")
    var index = ddl.selectedIndex;
     
    var employee_name = ddl.options[index].text;

    var AddData = "{";

    AddData += '"PRODUCE_ID": ' + id + ',';
    //工序
    AddData += '"PROGRESS_NAME":"' + $("#PROGRESS").val() + '",';

    //数量
    AddData += '"PROGRESS_COUNT":"' + $("#PROGRESS_COUNT").val() + '",';

    //员工ID
    AddData += '"EMPLOYEE_ID":' + $("#drpstaff").val() + ',';

    //员工名称
    AddData += '"EMPLOYEE_NAME":"' + employee_name + '",';

    //开始时间
    AddData += '"PROCESS_START_TIME":"' + $("#PROCESS_START_TIME").val() + '",';
    //结束时间
    AddData += '"PROCESS_END_TIME":"' + $("#PROCESS_END_TIME").val() + '",';

    //备注
    AddData += '"DESCRIPTION":"' + $("#txtRemark").val() + '"';


    AddData += "}";

    return AddData;
};
function SaveProgressInfo() {
    if (confirm("确定保存吗？")) {
        $.post('/ProduceProgress/SaveProgressInfo',
            {
                progressInfo: GetData()

            }, function (data) {
                alert(data)
                if (data == "保存成功") {
                    $("#btnSave").attr("disabled", true);
                    window.location.href = '/ProduceProgress/Index';
                }
            }, 'json'
          );
    }
};

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
};