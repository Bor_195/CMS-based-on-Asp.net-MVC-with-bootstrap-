﻿$(document).ready(function () {
    $("tr").bind('click', function () {
        
        $("#p_b_body").html("");
        
        getAbsPoint($(this).get(0));
    });
});
function CellClick(id) {
    $("#p_b_body").html("");
    $.ajax({
        type: "POST",
        url: "/ProduceProgress/GetProgressData?id=" + id, //将后台操作放在此文件里
        data: "id=" + id,
        success: function (data) {
            var dataArray = eval(data.rows);
            if (data.rows == null) {
                $("#divProgress").animate({ opacity: "hide" }, 300);
                return;
            }
            else
            {
                var tableStr = "<table class=\"table table-striped table-hover table-bordered\" >";
                tableStr = tableStr + "<thead><td>进度</td><td>负责人</td><td>开始时间</td><td>结束时间</td><td>耗时</td></thead>";
                var len = dataArray.length;

                for (var i = 0 ; i < len ; i++) {
                    tableStr = tableStr + "<tr><td>" + dataArray[i].PROGRESS_NAME + "</td>" + "<td>" + dataArray[i].EMPLOYEE_NAME + "</td>" + "<td>" + renderTime(dataArray[i].PROCESS_START_TIME) + "</td>" + "<td>" + renderTime(dataArray[i].PROCESS_END_TIME) + "</td>" + "<td>" + dataArray[i].TIMESPAN + "</td></tr>";
                }
                tableStr = tableStr + "</table>";
                //将动态生成的table添加的事先隐藏的div中.  
                //$("#dataTable").html(tableStr);

                $("#p_b_body").html(tableStr);
                $("#divProgress").show();
            }
            
        }
    });
}
// 取得空间的坐标位置
function getAbsPoint(e) {
    var x = e.offsetLeft, y = e.offsetTop;
    while (e = e.offsetParent) {
        x += e.offsetLeft;
        y += e.offsetTop;
    }

    $("#divProgress").css({
        top: y,
        left: x + 400,
        width: '600px',
        height: '360px'
    });
}

function hideDiv(div_id) {
    $("#" + div_id + "").animate({ opacity: "hide" }, 300);
}


function renderTime(data) {
    var da = eval('new ' + data.replace('/', '', 'g').replace('/', '', 'g'));
    return da.getFullYear() + "-" + da.getMonth() + "-" + da.getDay() + "-" + da.getHours() + ":" + da.getSeconds() + ":" + da.getMinutes();
}