﻿var MB = window["MB"] || {}
MB.page = MB["page"] || {}
MB.page.comDetail = (function ($) {
    var comCount = 0
    var verCount = 0
    var disableVer = function (comId) {
        $('a[data-id="' + comId + '"][data-type="confirmver"]').attr('disabled', 'disabled')
        $('a[data-id="' + comId + '"][data-type="cancelver"]').attr('disabled', 'disabled')
    }
    var disableOrder = function (comId) {
        $("a[data-type='confirmOrder']").attr('disabled', 'disabled')
        $("a[data-type='cancelOrder']").attr('disabled', 'disabled')
    }
    return {
        'disableVer': disableVer,
        'disableOrder': disableOrder,
        'comCount':comCount,
        'verCount':verCount
    }
}(jQuery))

$(document).ready(function () {
    MB.page.comDetail.comCount = $("tr[data-type='com']").length

    $.each($("a[data-type='confirmver']"), function (index, node) {
        if ($(node).attr('disabled') === 'undefined') {
            MB.page.comDetail.verCount++
        }
    })
    console.log(MB.page.comDetail.comCount)
    console.log(MB.page.comDetail.verCount)


    $("#comDetail tr[data-type='com']").click(function () {
        if ($(this).attr("data-type") === "com") {
            $("div[data-type='comInfo']").hide();
            $("#comInfo_" + $(this).attr("data-id")).show();
        }
    })
    $("#comInfo_" + $("#comDetail tr[data-type='com']:first").attr("data-id")).show()
    //确认版型文件
    $("a[data-type='confirmver']").click(function () {
        if ($(this).attr('disabled') === 'disabled') return;
        if (confirm("确认版型文件合规吗?")) {
            var comId = $(this).attr('data-id')
            var preProduceId = $('#btnConfirmOrder').attr('data-id')

            $.post('/PreProduce/UpdateVersionFileStatus',
                {
                    PRE_COM_DETAIL_ID: comId,
                    PRE_PRODUCE_ID: preProduceId,
                    STATUS: 2
                }, function (data) {
                    alert(data)
                    if (data == "更新成功") {
                        MB.page.comDetail.verCount++
                        $('td[data-id="' + comId + '"]').text('已确认');
                        //如果版型全部确认,改变预生产单状态
                        if (MB.page.comDetail.verCount == MB.page.comDetail.comCount) {
                            $("a[data-type='confirmOrder']").removeAttr('disabled')
                        }
                        MB.page.comDetail.disableVer(comId)
                    }
                },
                "json"
                )
        }
    })
    //驳回版型文件
    $("a[data-type='cancelver']").click(function () {
        if ($(this).attr('disabled') === 'disabled') return;
        var comId = $(this).attr('data-id')
        if (confirm("确定要驳回版型文件吗?")) {
            $("#verRefuse_" + comId).modal('show')
        } else {
            $("#verRefuse_" + comId).modal('hide')
        }




    })
    $("button[data-type='verRefuseConfirm']").click(function () {
        var comId = $(this).attr('data-id')
        var preProduceId = $('#btnConfirmOrder').attr('data-id')
        var orderCode = $('#orderCode').text()
        $.post('/PreProduce/UpdateVersionFileStatus',
            {
                CANCEL_REASON: $("#texRefuse_" + comId).val(),
                PRE_COM_DETAIL_ID: comId,
                PRE_PRODUCE_ID: preProduceId,
                ORDER_CODE:orderCode,
                STATUS: 3
            }, function (data) {
                alert(data)
                if (data == "更新成功") {
                    $('td[data-id="' + comId + '"]').text('已驳回');
                    MB.page.comDetail.disableVer(comId)
                }
                $("#verRefuse_" + comId).modal('hide')
            }, 'json'
            )
    })
    $("button[data-type='verRefuseCancel']").click(function () {
        var comId = $(this).attr('data-id')
        $("#verRefuse_" + comId).modal('hidden')
    })
    //确认订单
    $("a[data-type='confirmOrder']").click(function () {
        if ($(this).attr('disabled') === 'disabled') return;
        if (confirm("确定要同意订单吗")) {
            var preProduceId = $('#btnConfirmOrder').attr('data-id')
            var requestData = {
                ID: [preProduceId],
                PRODUCE_STATUS: 3
            }

            $.post('/PreProduce/UpdatePreProduceStatus',
                { rawRequest: JSON.stringify(requestData) }
                , function (data) {
                    alert(data)
                    if (data == "更新成功") {
                        $('a[data-type="confirmver"]').attr('disabled', 'disabled')
                        $('a[data-type="cancelver"]').attr('disabled', 'disabled')
                        $("a[data-type='confirmOrder']").attr('disabled', 'disabled')
                    }
                }, 'json'
                )
        }
    })
    //驳回订单
    $("a[data-type='cancelOrder']").click(function () {
        if ($(this).attr('disabled') === 'disabled') return;
        if (confirm("确定要驳回生产单吗？")) {
            $('#myRefuse').modal('show');
            //MB.page.comDetail.disableVer()
            // MB.page.comDetail.disableOrder()
        }
        else {
            $('#myRefuse').modal('hide');

        }
    })
    $('#btnConfirmRefuse').click(function () {
        var preProduceId = $('#btnConfirmOrder').attr('data-id')
        var requestData = {
            ID: [preProduceId],
            PRODUCE_STATUS: 9,
            CANCEL_REASON: $('#texRefuse').val(),
            ORDER_CODE: [$('#orderCode').text()]
        }
        $.post("/PreProduce/UpdatePreProduceStatus"
            , { rawRequest: JSON.stringify(requestData) }
            , function (data) {
                alert(data)
                if (data == "更新成功") {
                    $('a[data-type="confirmver"]').attr('disabled', 'disabled')
                    $('a[data-type="cancelver"]').attr('disabled', 'disabled')
                    MB.page.comDetail.disableOrder()
                    $('#myRefuse').modal('hide');
                }
            }, "json")
    })
    $('.modal-body input[type="checkbox"]').click(function () {
        console.log($(this).attr('checked'))
        if ($(this).attr('checked') === "checked") {
            console.log($('textarea[data-id="' + $(this).attr('data-bind') + '"]').val())
            var remark = $('textarea[data-id="' + $(this).attr('data-bind') + '"]').val()
            var selectTextArea = 'textarea[data-id="' + $(this).attr('data-bind') + '"]'

            $(selectTextArea).val(remark
                + '\n' + "文件名:" + $(this).attr('ver-name') + ",版本号:" + $(this).attr('ver-code') + ", 下载地址:" + $(this).attr('ver-file'))
        }
    })

});

