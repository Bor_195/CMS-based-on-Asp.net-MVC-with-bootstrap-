﻿$(function () {
    InitEvent();
    Query();
});

function InitEvent() {
    $('#bs_module_tree').on("changed.jstree", function (e, data) {
        if (data.selected != "") {
            $.ajax({
                url: "/BsModule/GetBsModuleById?id=" + data.node.id + "",
                type: "post",
                success: function (newData) {
                    eval("newData=" + newData);
                    if (newData != null && newData != "") {
                        if (data.node.parent == "#")
                            document.getElementById("parent").innerHTML = "";
                        else
                            document.getElementById("parent").innerHTML = data.instance.get_node(data.node.parent).text;
                        document.getElementById("ID").value = newData.ID;
                        document.getElementById("ParentID").value = newData.ParentID;
                        document.getElementById("ModuleCode").value = newData.ModuleCode;
                        document.getElementById("ModuleName").value = newData.ModuleName;
                        document.getElementById("Remark").value = newData.Remark == null ? "" : newData.Remark;
                        document.getElementById("Creator").value = newData.Creator;
                        document.getElementById("CreateTime").value = newData.CreateTime;
                        document.getElementById("ModuleUrl").value = newData.ModuleUrl;
                        document.getElementById("ModuleOrder").value = newData.ModuleOrder;
                        document.getElementById("ModuleIcon").value = newData.ModuleIcon == null ? "" : newData.ModuleIcon;
                    }
                }
            });
        }
    });
}

//新增子级
$('#addChild').click(function () {
    var ref = $('#bs_module_tree').jstree(true),
         sel = ref.get_selected();
    if (!sel.length) {
        return false;
    }
    var obj = ref._model.data[Number(sel[0])];
    document.getElementById("parent").innerHTML = obj.text;
    document.getElementById("ParentID").value = obj.id;
    ReloadParameters();
});

//清空
$('#clear').click(function () {
    document.getElementById("parent").innerHTML = "";
    document.getElementById("ParentID").value = "";
    document.getElementById("Creator").value = "";
    document.getElementById("CreateTime").value = "";
    ReloadParameters();
});

//删除
$('#delete').click(function () {

    var ref = $('#bs_module_tree').jstree(true),
        sel = ref.get_selected();
    if (!sel.length) {
        return false;
    }
    var obj = ref._model.data[Number(sel[0])];
    var msg = "";
    if (obj.children.length > 0) {
        alert("请先删除此菜单下的子菜单！");
        return;
        //msg = "你确定要删除此菜单以及子菜单吗？";
    }
    else
        msg = "你确定要删除此菜单吗？";

    var conditions = {};
    conditions["id"] = obj.id;
    if (confirm(msg)) {
        $.post("DeleteBsModule", conditions, function (message) {
            if (message == "OK") {
                alert("删除成功！");
                ReloadTree();
            }
            else
                alert("删除失败！");
        }, "text");
    }
});

//保存
$('#save').click(function () {
    SaveMenu(document.getElementById("ParentID").value);
}); 
$("#J-switch").unbind("click").click(function () {
    var orgTree = $('#bs_module_tree');

    if (this.value.indexOf(' - 收起') > -1) {
        orgTree.jstree('close_all');
        $(this).val(' + 展开');
    } else {
        orgTree.jstree('open_all');
        $(this).val(' - 收起');
    }
});

//查询数据
function Query() {
    $.ajax({
        url: "/BsModule/GetBsModuleJsTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log(data)
            $("#bs_module_tree").jstree({
                "async": false,
                "cache": false,
                "core": {
                    "themes": {
                        "responsive": true
                    },
                    // so that create works
                    "check_callback": true,
                    'data': data
                },
                "types": {
                    "default": {
                        "icon": "fa fa-folder icon-state-warning icon-lg"
                    },
                    "file": {
                        "icon": "fa fa-file icon-state-warning icon-lg"
                    }
                },
                "plugins": ["types"]
            });
        },
    });
}

function SaveMenu(parId) {
    if ($("#ModuleCode").val() == "") {
        alert("模块编码不能为空");
        return false;
    } else if ($("#ModuleCode").val().replace(/[^x00-xff]/ig, 'aa').length > 50) {
        alert("模块编码长度不能超过50(一个中文相当于两个)");
        return false;
    }
    if ($("#ModuleName").val() == "") {
        alert("模块名称不能为空");
        return false;
    } else if ($("#ModuleName").val().replace(/[^x00-xff]/ig, 'aa').length > 50) {
        alert("模块名称长度不能超过50(一个中文相当于两个)");
        return false;
    }
    if ($("#ModuleIcon").val().replace(/[^x00-xff]/ig, 'aa').length > 50) {
        alert("图标长度不能超过50(一个中文相当于两个)");
        return false;
    }
    if ($("#Remark").val().replace(/[^x00-xff]/ig, 'aa').length > 100) {
        alert("备注长度不能超过100(一个中文相当于两个)");
        return false;
    }
    var conditions = {};
    var id = $("#ID").val();
    var code = $("#ModuleCode").val();
    var name = $("#ModuleName").val();
    var parentId = parId;
    var remark = $("#Remark").val();
    var createUser = $("#Creator").val();
    var createDate = $("#CreateTime").val();
    var url = $("#ModuleUrl").val();
    var order = $("#ModuleOrder").val();
    var icon = $("#ModuleIcon").val();    
    $.ajax({
        url: "/BsModule/SetBsModule",
        type: "post",
        dataType: "json",
        async: false,
        data: "id=" + id + "&code=" + code + "&name=" + name + "&parentId=" + parentId + "&remark=" + remark +
            "&createUser=" + createUser + "&createDate=" + createDate + "&url=" + url + "&order=" + order + "&icon="+icon,
        success: function (s) {
            if (s.IsSuccess == true) {
                ReloadTree();
            }
            else
                alert(s.ErrorMessage);
        },
    });



}

function ReloadTree() {
    $("#bs_module_tree").jstree(true).destroy();
    Query();
    InitEvent();
    document.getElementById("parent").innerHTML = "";
    document.getElementById("ParentID").value = "";
    document.getElementById("Creator").value = "";
    document.getElementById("CreateTime").value = "";
    ReloadParameters();
}

function ReloadParameters() {
    document.getElementById("ID").value = "";
    document.getElementById("ModuleCode").value = "";
    document.getElementById("ModuleName").value = "";
    document.getElementById("Remark").value = "";
    document.getElementById("ModuleUrl").value = "";
    document.getElementById("ModuleOrder").value = "";
    document.getElementById("ModuleIcon").value = "";
}