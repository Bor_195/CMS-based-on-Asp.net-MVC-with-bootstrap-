﻿var MB = window["MB"] || {}
MB.page = MB["page"] || {}
$(document).ready(function () {
    window.onkeydown = function (e) {
        if (e.keyCode === 13) {
            var option = {
                popClose: false
                , extraCss: ""
                , retainAttr: ["class", "id", "style", "on"]
                , extraHead: '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>'
            }
            $(".print-area").printArea(option)
        }
    }
    $(".printer").click(function () {
        var option = {
            popClose: true
    , extraCss: ""
    , retainAttr: ["class", "id", "style", "on"]
    , extraHead: '<meta charset="utf-8" />,<meta http-equiv="X-UA-Compatible" content="IE=edge"/>'
        }
        $(".print-area").printArea(option)
    })
    $("#btnPrintCancel").click(function () {
        window.close()
    })
    
    $(".printer").trigger("click")
})