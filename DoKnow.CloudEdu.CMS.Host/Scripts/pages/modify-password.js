﻿
$(document).ready(function () {
    // 弹出错误信息
    var result = $("#result-msg").val();
    if (typeof result != "undefined" && result.length > 0) {
        alert(result);
    }

    // 提交
    $("#btn-submit").click(function () {
        if ($("#oldPassWord").val() == "") {
            $("#oldPassWord").addClass("error");
            alert("请输入旧密码！");
            return false;
        }
        $("#oldPassWord").removeClass("error");
        if ($("#password").val().trim().length < 6) {
            $("#password").addClass("error");
            alert("新密码最少六位！");
            return false;
        }
        $("#password").removeClass("error");
        if ($("#confirmPassword").val() != $("#password").val()) {
            $("#confirmPassword").addClass("error");
            alert("请确认新密码！");
            return false;
        }
        $("#confirmPassword").removeClass("error");

        //try {
        // $("#pwd_form").validate({
        //errorPlacement: function (error, element) {
        //    $(element).addClass("error");
        //},
        //rules: {
        //    oldPassWord: {
        //        required: true
        //    },
        //    password: {
        //        required: true, 
        //        minlength:6 
        //    },
        //    confirmPassword: {
        //        equalTo: "#password"
        //    }
        //}
        // });
        //}
        //catch (ex) {
        //    console.log(ex);
        //}

    });

});
