﻿var MB = window["MB"] || {}
MB.page = MB["page"] || {}
MB.page.sysTypes = (function ($) {
    //查询数据
    var Query = function() {
        $.ajax({
            url: "/SysTypes/GetBaseTypeTree",
            type: "post",
            dataType: "json",
            success: function (data) {
                console.log(data)
                $("#bs_module_tree").jstree({
                    "async": false,
                    "cache": false,
                    "core": {
                        "themes": {
                            "responsive": true
                        },
                        // so that create works
                        "check_callback": true,
                        'data': data
                    },
                    "types": {
                        "default": {
                            "icon": "fa fa-folder icon-state-warning icon-lg"
                        },
                        "file": {
                            "icon": "fa fa-file icon-state-warning icon-lg"
                        }
                    },
                    "plugins": ["types"]
                });
            },
        });
    }
    var InitEvent = function() {
        $('#bs_module_tree').on("changed.jstree", function (e, data) {
            if (data.selected != "") {
               
                getMenuInfo(data.node)
            }
        });
    }
    var reloadTree = function () {
        $("#bs_module_tree").jstree(true).destroy();
        Query();
        InitEvent();
    }
    Query();
    InitEvent();
    var getMenuInfo = function (node) {
        $('#save').attr("disabled", false)
        $('#res-message').text("")
        $('#isUpdate').val(1)
        console.log(node)
        if (node.parent === "#") {
            setBaseType(node);
        }
        else {
            setBaseParameter(node);
        }
    }
    var addTypeItem = function () {
        if ($('#id').val() === "") {//未选中
            //add type
            resetType()
        } else if ($('#id').val() === "0") {//选中根节点
            //add type
            resetType()
        } else if ($('#parentId').val() === "0") {//选中父类
            //add para
            resetPara($('#value').val(),$('#id').val())
        } else {//选中子元素
            //add para
            resetPara($('#parentName').val(),$("#parentId").val())
        }
    }
    var resetType = function () {
        $('#id').val("")
        $('#code').val("")
        $('#desc').val("")
        $('#value').val("")
        $('#parentId').val(0);
        $('#order').val("");
        $('#parentName').val("");
        $('#order').attr('disabled', true);
    }
    var resetPara = function (parentName, parentId) {
        $('#id').val("")
        $('#code').val("")
        $('#desc').val("")
        $('#value').val("")
        $('#order').val("")
        $("#parentId").val(parentId)
        $('#parentName').val(parentName)
        $('#order').attr('disabled', false);
    }
    var reset = function () {
        console.log($('#code').val())
        $('#id').val("")
        $('#code').val("")
        $('#desc').val("")
        $('#value').val("")
        $('#parentId').val(0)
        $('#order').val("")
        $('#order').attr('disabled', false);
    }
    var setBaseType = function (node) {
        $('#order').attr('disabled', true)
        $('#parentName').val("")
        $('#order').val("")
        $('#id').val(node.id)
        $('#parentId').val(0);
        $('#code').val(node.data.type_code)
        $('#desc').val(node.data.type_desc)
        $('#value').val(node.data.type_name)

    }
    var setBaseParameter = function (node) {
        $('#parentName').val(node.original.parentname)
        $('#id').val(node.id)
        $('#code').val(node.data.para_code)
        $('#desc').val(node.data.para_desc)
        $('#value').val(node.data.para_value)
        $('#order').val(node.data.para_order)
        $('#parentId').val(node.parentid)
        $('#order').attr('disabled', false);
    }
    return {
        "getMenuInfo": getMenuInfo,
        "addTypeItem": addTypeItem,
        "reset": reset,
        "reloadTree": reloadTree
    }
}(jQuery))

$(document).ready(function () {
    // 弹出错误信息
    var result = $("#result-msg").val();
    
    if (typeof result != "undefined" && result.length > 0) {
        $('#res-message').text(result)
        MB.page.sysTypes.reset()
        // window.location.href = location.href;
       // $('#menu-tree').tree('reload')
    }
    // 默认为修改状态
    $("#isUpdate").val("0");
    // 提交
    $("#save").click(function () {
        if ($("#value").val() == "") {
            alert("菜单名称不能为空！");
            return false;
        }
        if ($("#code").val() == "") {
            alert("菜单编码不能为空！");
            return false;
        }
    });
    // 清空操作
    $("#clear").click(function () {
        MB.page.sysTypes.reset();
    })
    // 添加操作
    $("#addChild").click(function () {
        $('#res-message').text("输入参数后点击保存")
        $("#isUpdate").val("0");
        MB.page.sysTypes.addTypeItem()
    });

    // 删除操作
    $("#delete").click(function () {
        $('#res-message').text('')
        if ($("#id").val() == "") {
            alert("请单击选择要删除的菜单项");
            return false;
        }
        if ($("#id").val() === "0") {
            alert("根节点无法删除！");
            return false;
        }
        if (!confirm("是否删除该菜单？")) {
            return;
        }
        var isParent = true;
        if ($("#parentId").val() !== "0") isParent = false;
        // post
        $.post("/SysTypes/DeleteMenuById", { id: $("#id").val(), isParent: isParent }).done(function (data) {
            $('#res-message').text(data)
            MB.page.sysTypes.reloadTree();
            MB.page.sysTypes.reset()
            //window.location.href = location.href;
        }).fail(function () { $('#res-message').text("删除失败") });
    });
    
    $("#J-switch").click(function () {
        var orgTree = $('#bs_module_tree');

        if (this.value.indexOf(' - 收起') > -1) {
            orgTree.jstree('close_all');
            $(this).val(' + 展开');
        } else {
            orgTree.jstree('open_all');
            $(this).val(' - 收起');
        }
    });
});

