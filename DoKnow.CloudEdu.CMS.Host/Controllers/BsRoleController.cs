﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DoKnow.CloudEdu.CMS.Host.Models;
using DoKnow.CloudEdu.Common.Attrs;
using DoKnow.CloudEdu.Common.Consts;
using DoKnow.CloudEdu.Common.Enums;
using DoKnow.CloudEdu.DataContract.Model.Base;
using DoKnow.Common.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Webdiyer.WebControls.Mvc;

namespace DoKnow.CloudEdu.CMS.Host.Controllers
{
    public class BsRoleController : BaseController
    {
        //
        // GET: /BsRole/

        public ActionResult Index(BSRolePageInfo request)
        {
            if (null != TempData["Msg"])
            {
                ViewBag.Msg = TempData["Msg"].ToString();
            }
            return QueryBsRole(request);
        }
        [MenuItem("~/BsRole/Index")]
        public ActionResult QueryBsRole(BSRolePageInfo request)
        {
            RoleDatas roles = HttpHelper.Post<RoleDatas>(Server + ServicePath.RoleRead,
                JsonConvert.SerializeObject(
                new
                {
                    ActionName = "RetrieveRoles",
                    Pars =
                      new
                      {
                          role = EmitHelper.Map<BSRolePageInfo, BSRoleInfo>(request),
                          page = EmitHelper.Map<BSRolePageInfo, PageInfo>(request)
                      }
                }));
            if (null != roles && null != roles.RoleEntitys && roles.RoleEntitys.Count > 0)
            {
                var model = new PagedList<BSRolePageInfo>(EmitHelper.Map<BSRoleInfo, BSRolePageInfo>(roles.RoleEntitys), request.PageIndex, request.PageSize, roles.Total);
                return View("Index", model);
            }
            return View("Index"); 
        }
        [MenuItem("~/BsRole/Index")]
        public ActionResult Edit(int id,string roleCode,string roleName)
        {
            var model = HttpHelper.Post<BSRoleInfo>(Server + ServicePath.RoleRead,
                    JsonConvert.SerializeObject(
                    new
                    {
                        ActionName = "RetrieveRole",
                        Pars =
                          new
                          {
                              role = new BSRoleInfo() { ID = id }
                          }
                    }));
            if (model != null)
            {
                ViewBag.ComStatus = EnumHelper.GetSelectList<EnumHelper.CommonStatus>(model.Status.ToString());
                return View("Edit", model);
            }
            else
            {
                TempData["Msg"] = "该角色已不存在！";
                return RedirectToAction("Index", new { RoleCode = roleCode, RoleName = roleName });
            }
        }
        [MenuItem("~/BsRole/Index")]
        public ActionResult Add()
        {
            ViewBag.ComStatus = EnumHelper.GetSelectList<EnumHelper.CommonStatus>();
            return View("Add", new BSRoleInfo());
        }
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [MenuItem("~/BsRole/Index")]
        [HttpPost]
        public ActionResult AddSave(BSRoleInfo model)
        {
            //var managerClient = new BsRoleManagerClient();
            model.Creator = model.Modifier = CurrentUser.UserEntity.Code;
            model.CreateTime = model.ModifyTime = System.DateTime.Now;
            //model.STATUS = (int)EnumHelper.CommonStatus.Active;
            var response = HttpHelper.Post(Server + ServicePath.RoleWrite,
                    JsonConvert.SerializeObject(
                    new
                    {
                        ActionName = "CreateRole",
                        Pars =
                          new
                          {
                              role = model
                          }
                    }, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" }));
            if (!response.IsSuccess)
            {
                ViewBag.ComStatus = EnumHelper.GetSelectList<EnumHelper.CommonStatus>(model.Status.ToString());
                ViewBag.ErrMsg = string.IsNullOrEmpty(response.ErrMsg) ? string.Empty : (response.ErrMsg + ",") + "新增失败！";
                return View("Add", model);
            }
            else
            {
                TempData["Msg"] = "新增成功！";
                return RedirectToAction("Index");
            }
        }
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [MenuItem("~/BsRole/Index")]
        [HttpPost]
        public ActionResult EditSave(BSRoleInfo model)
        {
            model.Modifier = CurrentUser.UserEntity.Code;
            model.ModifyTime = System.DateTime.Now;
            //model.STATUS = (int)EnumHelper.CommonStatus.Active;
            var response = HttpHelper.Post(Server + ServicePath.RoleRead,
                    JsonConvert.SerializeObject(
                    new
                    {
                        ActionName = "UpdateRole",
                        Pars =
                          new
                          {
                              role = model
                          }
                    }, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" }));
            if (!response.IsSuccess)
            {
                ViewBag.ErrMsg = string.IsNullOrEmpty(response.ErrMsg) ? string.Empty : (response.ErrMsg + ",") + "编辑失败！";
                ViewBag.ComStatus = EnumHelper.GetSelectList<EnumHelper.CommonStatus>(model.Status.ToString());
                return View("Edit", model);
            }
            else
            {
                TempData["Msg"] = "编辑成功！";
                return RedirectToAction("Index");
            }
        }

        [MenuItem("~/BsRole/Index")]
        public ActionResult Delete(int id)
        {
            var response = HttpHelper.Post(Server + ServicePath.RoleWrite,
                JsonConvert.SerializeObject(
                new
                {
                    ActionName = "DeleteRole",
                    Pars =
                      new
                      {
                          ID = id
                      }
                }));
            if (response.IsSuccess)
            {
                TempData["Msg"] = "删除成功！";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["Msg"] = string.IsNullOrEmpty(response.ErrMsg) ? string.Empty : (response.ErrMsg + ",") + "删除失败！";
                return RedirectToAction("Index");
            }
        }
        public ActionResult GetRoleStatus()
        {
            var status = EnumHelper.GetList(typeof(EnumHelper.CommonStatus));
            var result = Json(status);
            return result;
        }
    }
}
