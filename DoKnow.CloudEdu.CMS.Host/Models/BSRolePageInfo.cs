﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using DoKnow.CloudEdu.DataContract.Model.Base;

namespace DoKnow.CloudEdu.CMS.Host.Models
{
    public class BSRolePageInfo : BSRoleInfo
    {
        //public bool IsDelete { get; set; }
        private int _PageIndex = 1;
        public int PageIndex
        {
            get { return _PageIndex;}

            set {_PageIndex = value;}
        }
        private int _PageSize = 20;
        public int PageSize { 
            get { return _PageSize;}
            set { _PageIndex = value;}
        }
    }
}