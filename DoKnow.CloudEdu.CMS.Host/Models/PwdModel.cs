﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoKnow.CloudEdu.CMS.Host.Models
{
    public class PwdModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string OldPWD { get; set; }
        public string NewPWD { get; set; }
        public string ConfirmPWD { get; set; }
    }
}