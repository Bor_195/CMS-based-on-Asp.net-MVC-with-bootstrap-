﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoKnow.CloudEdu.CMS.Host.Models
{
    public class PageHeader
    {
        public string Title { get; set; }
        public Dictionary<string, string> Urls { get; set; }
    }
}